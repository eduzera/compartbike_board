require 'spec_helper'

describe CompartbikeBoard::Board do
  let!(:board) { CompartbikeBoard::Board.new('/dev/tty.usbserial', 9600) }
  subject { board }

  before(:all) { board.connect! }
  after(:all) { subject.close! }

  context 'when connect' do
    it 'should have valid connection' do
      subject.connect!.should be_true
      subject.should_not be_closed
    end
  end

  context 'when reconect' do
    it 'should reconect' do
      subject.reconnect!
      subject.should_not be_closed
    end
  end
end
