require 'spec_helper'

describe CompartbikeBoard::Commands do
  let!(:board) { CompartbikeBoard::Board.new('/dev/tty.usbserial', 9600) }

  context 'with a board' do
    before { board.connect! }
    after { board.close! }

    context 'when reset' do
      it 'should get success code after reset' do
        response = CompartbikeBoard::Commands.reset
        response.should be_success
      end
    end

    context 'when release bike' do
      it 'should release and get success code' do
        response = CompartbikeBoard::Commands.release_bike
        response.should be_success
      end
    end

    context 'when turn on/off leds' do
      it 'should turn on green led' do
        response = CompartbikeBoard::Commands.turn_on(:led_green)
        response.should be_success
      end

      it 'should turn off green led' do
        response = CompartbikeBoard::Commands.turn_off(:led_green)
        response.should be_success
      end

      it 'should turn on red led' do
        response = CompartbikeBoard::Commands.turn_on(:led_red)
        response.should be_success
      end

      it 'should turn off red led' do
        response = CompartbikeBoard::Commands.turn_off(:led_red)
        response.should be_success
      end
      it 'should turn on blue led' do
        response = CompartbikeBoard::Commands.turn_on(:led_blue)
        response.should be_success
      end

      it 'should turn off blue led' do
        response = CompartbikeBoard::Commands.turn_off(:led_blue)
        response.should be_success
      end

      it 'should turn on buz' do
        response = CompartbikeBoard::Commands.turn_on(:buz)
        response.should be_success
      end

      it 'should turn off buz' do
        response = CompartbikeBoard::Commands.turn_off(:buz)
        response.should be_success
      end

      it 'should turn on buz for 2 seconds' do
        response = CompartbikeBoard::Commands.turn_on(:buz, 1, 2)
        response.should be_success
      end
    end

    context 'when falsh leds and buz' do
      it 'should flash 10 times green led' do
        response = CompartbikeBoard::Commands.flash(:led_green, 10)
        response.should be_success
      end

      it 'should flash 10 times red led' do
        response = CompartbikeBoard::Commands.flash(:led_red, 10)
        response.should be_success
      end

      it 'should flash 10 times blue led' do
        response = CompartbikeBoard::Commands.flash(:led_blue, 10)
        response.should be_success
      end

      it 'should flash 3 times buz' do
        response = CompartbikeBoard::Commands.flash(:buz, 3)
        response.should be_success
      end
    end
  end

  context 'with a stubbed board' do
    before { $board = double(:board) }

    describe '#send_data' do
      before { $board.stub(:send_data).and_raise(EOFError) }

      it 'should raise error CompartbikeBoard::Error when it receives EOFError' do
        expect { CompartbikeBoard::Commands.check_status(1000) }.to raise_error(CompartbikeBoard::NoResponseError)
      end
    end

    describe '#check_status' do
      before { $board.stub(send_data: data) }
      subject { CompartbikeBoard::Commands.check_status }

      context 'when status return bike but with locker error' do
        let(:data) { "\x02\x01\x000\v\x00AC00033A5Fu\n" }

        its(:bike) { should == 'AC00033A5F' }
        its(:sensor_status) { should == 'error' }
      end

      context 'when status not return bike and have locker error' do
        let(:data) { "\x02\x01\x000\v\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00>\n" }

        its(:bike) { should be_nil }
        its(:sensor_status) { should == 'error' }
      end
    end
  end
end
