require 'serialport'

require 'compartbike_board/error'
require 'compartbike_board/board'
require 'compartbike_board/code'
require 'compartbike_board/commands'

require 'compartbike_board/parser'
require 'compartbike_board/response'
require 'compartbike_board/response_status'
