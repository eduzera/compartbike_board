module CompartbikeBoard
  class ResponseStatus
    attr_accessor :lock_status, :bike
    
    def initialize(lock_status, bike)
      @lock_status = lock_status
      @bike = bike
      @bike = nil if @bike == "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
    end

    def sensor_status
      case self.lock_status
      when CompartbikeBoard::Code::SENSORS[:has_bike]
        'has_bike'
      when CompartbikeBoard::Code::SENSORS[:has_not_bike]
        'has_not_bike'
      else
        'error'
      end
    end
  end
end