module CompartbikeBoard
  class Error < StandardError
  end

  class NoResponseError < Error
    def message
      'Expected a response data from board, but did not receive any.'
    end
  end
end