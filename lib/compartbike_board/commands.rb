module CompartbikeBoard
  class Commands
    @queue = :commands

    def self.perform(method, *args)
      send(method, *args)
    end

    def self.check_status(slot_name=1)
      send_data(slot_name: slot_name, function: CompartbikeBoard::Code::STATUS, bytes: 00)
    end

    def self.reset(slot_name=1)
      send_data(slot_name: slot_name, function: CompartbikeBoard::Code::RESET, bytes: 00)
    end

    def self.release_bike(slot_name=1)
      sleep(0.2)
      send_data(slot_name: slot_name, function: CompartbikeBoard::Code::RELEASE, bytes: 00)
    end

    def self.turn_on(device, slot_name=1, seconds=0)
      response = send_data(slot_name: slot_name, function: CompartbikeBoard::Code::DEVICES[device.to_sym], bytes: 01, data: 01)
      
      CompartbikeBoard::Commands.wait_seconds_to_call(seconds){  CompartbikeBoard::Commands.turn_off(device, slot_name)  }
      
      response
    end

    def self.turn_off(device, slot_name=1)
      send_data(slot_name: slot_name, function: CompartbikeBoard::Code::DEVICES[device.to_sym], bytes: 01, data: 00)
    end

    def self.flash(device, times, slot_name=1)
      send_data(slot_name: slot_name, function: CompartbikeBoard::Code::FLASH_DEVICES[device.to_sym], bytes: 01, data: times)
    end

    protected 
      def self.send_data(options)
        data = build_pack(options)
        unparsed_response = $board.send_data(data)
        Parser.new(unparsed_response).response
      rescue EOFError
        raise CompartbikeBoard::NoResponseError
      end

      def self.build_pack(options={})
        pack = [02, 00, options[:slot_name], options[:function], options[:bytes]]
        pack << options[:data] if options[:bytes] != 0
        pack << get_checksum(pack)
        pack << 0x0A
        pack
      end

      def self.get_checksum(pack)
        pack.inject(0){|sum, byte| sum + byte }
      end
      
      def self.wait_seconds_to_call(seconds)
         if seconds > 0
           sleep(seconds.to_i) 
           yield
         end
      end
  end
end
