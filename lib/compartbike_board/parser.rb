module CompartbikeBoard
  class Parser
    attr_accessor :data

    def initialize(data)
      @data = data
    end

    def response
      package = @data.chars.to_a

      if @data.include? Code::OK
        Response.new(package[package.index(Code::OK)])
      else
        lock_code = package[5].to_s.unpack('C*').join.to_i
        bike      = package[6..15].join
        ResponseStatus.new(lock_code, bike)
      end
    end
  end
end