# encoding: BINARY

require 'serialport'

module CompartbikeBoard

  class Board
    attr_accessor :conn, :port, :baud_rate, :data_bits, :stop_bits, :parity

    def initialize(port='/dev/tty.usbserial', baud_rate=9600)
      @port = port
      @baud_rate = baud_rate
      @data_bits = 8
      @stop_bits = 1
      @parity = SerialPort::NONE
    end

    def connect!
      return true unless closed?
      
      @conn = SerialPort.new(@port, @baud_rate, @data_bits, @stop_bits, @parity)
      @conn.read_timeout = 2 * 1000 #in miliseconds
      @conn.sync = true
      $board = self
      true
    end

    def reconnect!
      unless closed?
        close!
        connect!
      end
    end

    def closed?
      return true if @conn.nil?
      @conn.closed?
    end

    def close!
      return true if @conn.nil?
      @conn.close
      true
    end

    def read_line
      @conn.readline.force_encoding('BINARY')
    end

    def send_data(data)
      connect!
      @conn.write data.pack('C*')
      @conn.flush

      read_line
    end
  end
end
