module CompartbikeBoard
  class Code
    # FUNCTIONS CODE
    RELEASE        = 0x01 
    STATUS         = 0x02
    RESET          = 0x03
    DEVICES        = {led_green: 0x04, led_red: 0x06, led_blue: 0x05, buz: 0x07}
    FLASH_DEVICES  = {led_green: 0x08, led_red: 0x0A, led_blue: 0x09, buz: 0x0B}
    
    #RESPONSE
    OK  = [200].pack('C*')
    

    #SENSOR STATUS
    SENSORS = {has_bike: 1, has_not_bike: 2, error0: 0, error3: 3}
  end
end