module CompartbikeBoard
  class Response
    attr_accessor :code
    
    def initialize(code)
      self.code = code
    end

    def success?
      self.code == CompartbikeBoard::Code::OK
    end
  end
end